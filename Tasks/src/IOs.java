import java.io.BufferedReader;
import java.io.InputStreamReader;

public class IOs {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static double readDouble(){
        while(true) {
            try {
                double val = Double.valueOf(br.readLine());
                return val;
            } catch (Exception e) {
                System.out.println("Fehlerhafte Eingabe - Bitte Wiederholen!");
            }
        }

    }
 public static String readString(){
        while(true) {
            try {
                String val = br.readLine();
                return val;
            } catch (Exception e) {
                System.out.println("Fehlerhafte Eingabe - Bitte Wiederholen!");
            }
        }
    }
    public static int readInt(){
        while(true) {
            try {
                int val = Integer.valueOf(br.readLine());
                return val;
            } catch (Exception e) {
                System.out.println("Fehlerhafte Eingabe - Bitte Wiederholen!");
            }
        }

    }
}
